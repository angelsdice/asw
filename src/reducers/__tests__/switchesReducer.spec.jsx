import reducer from '../switchesReducer';
import * as types from '../../actions/actionTypes';

const initialState = {
  switches: [
    { name: 'goodSwitch', label: 'Good', isActive: false, color: 'red' },
    { name: 'fastSwitch', label: 'Fast', isActive: false, color: 'blue' },
    { name: 'cheapSwitch', label: 'Cheap', isActive: false, color: 'green' }
  ]
};

describe('switches reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it('should handle single TOGGLE_SWITCH', () => {
    expect(
      reducer(
        {
          ...initialState
        },
        {
          type: types.TOGGLE_SWITCH,
          payload: { name: 'goodSwitch' }
        }
      )
    ).toEqual({
      switches: [
        { name: 'goodSwitch', label: 'Good', isActive: true, color: 'red' },
        { name: 'fastSwitch', label: 'Fast', isActive: false, color: 'blue' },
        { name: 'cheapSwitch', label: 'Cheap', isActive: false, color: 'green' }
      ]
    });
  });

  it('should handle TOGGLE_SWITCH but no more active than 2', () => {
    expect(
      reducer(
        {
          switches: [
            { name: 'goodSwitch', label: 'Good', isActive: true, color: 'red' },
            {
              name: 'fastSwitch',
              label: 'Fast',
              isActive: true,
              color: 'blue'
            },
            {
              name: 'cheapSwitch',
              label: 'Cheap',
              isActive: false,
              color: 'green'
            }
          ]
        },
        {
          type: types.TOGGLE_SWITCH,
          payload: { name: 'cheapSwitch' }
        }
      )
    ).toEqual({
      switches: [
        { name: 'goodSwitch', label: 'Good', isActive: true, color: 'red' },
        { name: 'fastSwitch', label: 'Fast', isActive: false, color: 'blue' },
        { name: 'cheapSwitch', label: 'Cheap', isActive: true, color: 'green' }
      ]
    });
  });
});
