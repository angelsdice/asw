import { combineReducers } from 'redux';
import switches from './switchesReducer';
const rootReducer = combineReducers({
  switches
});

export default rootReducer;
