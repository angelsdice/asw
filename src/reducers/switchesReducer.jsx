import * as types from '../actions/actionTypes';

const initialState = {
  switches: [
    { name: 'goodSwitch', label: 'Good', isActive: false, color: 'red' },
    { name: 'fastSwitch', label: 'Fast', isActive: false, color: 'blue' },
    { name: 'cheapSwitch', label: 'Cheap', isActive: false, color: 'green' }
  ]
};

const switchesReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.TOGGLE_SWITCH:
      let tooMany = false;
      let indexToTurnOff;
      if (
        state.switches.filter(
          s => s.name === action.payload.name && s.isActive === false
        ).length >= 1 &&
        state.switches.filter(s => s.isActive === true).length >= 2
      ) {
        tooMany = true;
        indexToTurnOff =
          state.switches.findIndex(s => s.name === action.payload.name) - 1;
        if (indexToTurnOff < 0) {
          indexToTurnOff = state.switches.length - 1;
        }
      }

      return {
        ...state,
        switches: state.switches.map((sw, index) => {
          if (
            sw.name === action.payload.name ||
            (tooMany && indexToTurnOff === index)
          ) {
            return {
              ...sw,
              isActive: !sw.isActive
            };
          }
          return sw;
        })
      };
    default:
      return state;
  }
};

export default switchesReducer;
