import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Switches from '../../components/SwitchesComponents/Switches';
import * as SwitchesActions from '../../actions/SwitchesActions';

export class SwitchesContainer extends React.Component {
  render() {
    const { SwitchesActions, switches } = this.props;
    return <Switches SwitchesActions={SwitchesActions} switches={switches} />;
  }
}

export const mapStateToProps = state => {
  return {
    switches: state.switches.switches
  };
};

export const mapDispatchToProps = dispatch => ({
  SwitchesActions: bindActionCreators(SwitchesActions, dispatch)
});

const index = connect(
  mapStateToProps,
  mapDispatchToProps
)(SwitchesContainer);

export default index;
