import React from 'react';
import { SwitchesContainer } from '../SwitchesContainer';

describe('SwitchesContainer container', () => {
  it('should render correctly', () => {
    const wrapper = global.shallow(<SwitchesContainer />);
    expect(wrapper.find('WithStyles(Switches)').length).toBe(1);
  });
});
