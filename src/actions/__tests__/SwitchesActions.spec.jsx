import * as actions from '../SwitchesActions';
import * as types from '../actionTypes';

describe('switchesActions', () => {
  it('should create an action to toggleSwitch', () => {
    const expectedAction = {
      type: types.TOGGLE_SWITCH,
      payload: { name: 'goodSwitch' }
    };
    expect(actions.toggleSwitch('goodSwitch')).toEqual(expectedAction);
  });
});
