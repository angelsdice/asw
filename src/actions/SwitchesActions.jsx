import * as types from './actionTypes';

export const toggleSwitch = name => {
  return {
    type: types.TOGGLE_SWITCH,
    payload: {
      name
    }
  };
};
