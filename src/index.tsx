import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import ErrorBoundary from './components/SharedComponents/ErrorBoundary';
import * as serviceWorker from './serviceWorker';
import buildStore from './store/configureStore';
import Switches from './containers/SwitchesContainer';

import './index.scss';

const store = buildStore();

ReactDOM.render(
  <Provider store={store}>
    <ErrorBoundary>
      <Switches />
    </ErrorBoundary>
  </Provider>,
  document.getElementById('root')
);
serviceWorker.unregister();
