import React from 'react';
import Switches from '../../Switches';
function setup() {
  const props = {
    switches: [
      { name: 'goodSwitch', label: 'Good', isActive: false, color: 'red' },
      { name: 'fastSwitch', label: 'Fast', isActive: false, color: 'blue' },
      { name: 'cheapSwitch', label: 'Cheap', isActive: false, color: 'green' }
    ],
    SwitchesActions: {
      toggleSwitch: jest.fn()
    }
  };

  const enzymeWrapper = global.mount(
    <Switches
      switches={props.switches}
      SwitchesActions={props.SwitchesActions}
    />
  );
  return {
    props,
    enzymeWrapper
  };
}
describe('components', () => {
  describe('Switches', () => {
    it('should render self', () => {
      const { enzymeWrapper } = setup();
      expect(enzymeWrapper.find('FormGroup').length).toBe(1);
      expect(enzymeWrapper.find('FormControlLabel').length).toBe(3);
      expect(enzymeWrapper.find('Switch').length).toBe(3);
    });
    it('should call onChange prop', () => {
      const { enzymeWrapper, props } = setup();
      enzymeWrapper
        .find('Switch')
        .at(0)
        .props()
        .onChange();
      expect(props.SwitchesActions.toggleSwitch.mock.calls.length).toEqual(1);
      expect(props.SwitchesActions.toggleSwitch.mock.calls[0][0]).toEqual(
        'goodSwitch'
      );
    });
  });
});
