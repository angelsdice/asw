import React from 'react';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import { withStyles } from '@material-ui/styles';

const styles = theme => ({
  greenSwitchBase: {
    color: '#0f0',
    '&$colorChecked': {
      color: '#0f0',
      '& + $colorBar': {
        backgroundColor: '#0f0'
      }
    }
  },
  blueSwitchBase: {
    color: '#00f',
    '&$colorChecked': {
      color: '#00f',
      '& + $colorBar': {
        backgroundColor: '#00f'
      }
    }
  },
  redSwitchBase: {
    color: '#f00',
    '&$colorChecked': {
      color: '#f00',
      '& + $colorBar': {
        backgroundColor: '#f00'
      }
    }
  },

  colorBar: {},
  colorChecked: {}
});

const Switches = props => {
  const { switches, SwitchesActions, classes } = props;

  return (
    <FormGroup>
      {switches &&
        switches.map(sw => {
          let styleClass;
          switch (sw.color) {
            case 'green':
              styleClass = classes.greenSwitchBase;
              break;
            case 'blue':
              styleClass = classes.blueSwitchBase;
              break;
            case 'red':
              styleClass = classes.redSwitchBase;
              break;
            default:
              break;
          }
          return (
            <FormControlLabel
              key={sw.name}
              control={
                <Switch
                  onChange={() => SwitchesActions.toggleSwitch(sw.name)}
                  checked={sw.isActive}
                  value={sw.name}
                  classes={{
                    switchBase: styleClass,
                    checked: classes.colorChecked,
                    bar: classes.colorBar
                  }}
                  color="default"
                />
              }
              label={sw.label}
            />
          );
        })}
    </FormGroup>
  );
};
export default withStyles(styles)(Switches);
