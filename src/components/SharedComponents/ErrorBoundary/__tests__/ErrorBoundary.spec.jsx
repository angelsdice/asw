import React from 'react';
import ErrorBoundary from '../../ErrorBoundary';

function setup() {
  const enzymeWrapper = global.mount(
    <ErrorBoundary>
      <div>MychildComponent</div>
    </ErrorBoundary>
  );
  return {
    enzymeWrapper
  };
}
describe('components', () => {
  describe('ErrorBoundary', () => {
    it('should render correct children if no error', () => {
      const { enzymeWrapper } = setup();
      expect(enzymeWrapper.find(ErrorBoundary).length).toBe(1);
      expect(enzymeWrapper.find('div').length).toBe(1);
      expect(enzymeWrapper.find('div').text()).toEqual('MychildComponent');
    });
    it('generates a error message when an error is caught', () => {
      const { enzymeWrapper } = setup();
      enzymeWrapper.setState({
        hasError: true,
        error: 'error name',
        errorInfo: 'error info'
      });
      expect(enzymeWrapper.find('h1').length).toBe(1);
      expect(enzymeWrapper.find('h1').text()).toEqual('Something went wrong.');
    });
  });
});
