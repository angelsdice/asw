import React, { Component, ErrorInfo } from 'react';
import styles from './ErrorBoundary.module.scss';

type ErrorBoundaryState = {
  hasError: boolean;
};
interface ErrorBoundaryProps {
  children: any;
}

export default class ErrorBoundary extends Component<
  ErrorBoundaryProps,
  ErrorBoundaryState
> {
  constructor(props: any) {
    super(props);
    this.state = { hasError: false };
  }

  componentDidCatch(error: Error, info: ErrorInfo) {
    this.setState({ hasError: true });
    console.log(error, info);
  }

  render() {
    const { hasError } = this.state;
    const { children } = this.props;
    if (hasError) {
      // need to update the error message with something more meaningful
      return <h1 className={styles.heading}>Something went wrong.</h1>;
    }
    return children;
  }
}
