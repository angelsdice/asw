import React from 'react';
import loadingIcon from './loader.gif';
import styles from './Loader.module.scss';

type Props = {};

const Loader: React.SFC<Props> = () => (
  <div className={styles.container}>
    <img src={loadingIcon} alt="Loading..." />
  </div>
);

export default Loader;
